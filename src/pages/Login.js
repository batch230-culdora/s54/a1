import { Form, Button } from 'react-bootstrap';
import { useState, useEffect, useContext } from 'react';
import UserContext from '../UserContext';
import {Navigate} from 'react-router-dom';

export default function Login(){


	// Step 3 - Use the state
	// Variable, setter function

	const { user, setUser } = useContext(UserContext);

	//  state hooks to store the valuers of the input fields
	const [email, setEmail] = useState('');

	const [password1, setPassword1] = useState('');
	const [isActive, setIsActive] = useState(false);

	// Check if values are successfully binded
	console.log(email);
	console.log(password1);
	

	function LoginAccount(event){

		event.preventDefault();

		localStorage.setItem('email', email)
		
		setUser({
			email: localStorage.getItem('email')
		})

		// clear input fields
		setEmail('');
		setPassword1('');
		alert(`Welcome ${email}!`);
	}

	useEffect(()=>{
		if(email !== '' && password1 !== ''){
			setIsActive(true);
		}
		else{
			setIsActive(false);
		}
	}, [email, password1])


	return(

		(user.email !== null)
		? // true - means email field is successfully set
		<Navigate to ="/courses" />
		: // false - means email field is not successfully set
		 //<Form onSubmit={(event) => Login(event)}></Form>

		 <div className="container-fluid">
            <div className="row">
                <div className="col-md-4 col-sm-6 col-xs-12">
                   	<Form onSubmit = {(event) => LoginAccount(event)}>
						<h3>Login</h3>
            				<Form.Group controlId="userEmail">
               					 <Form.Label>Email address</Form.Label>
               						 <Form.Control 
	                							type="email" 
	                							placeholder="Enter email" 
	                							value = {email}
	                							onChange = {event => setEmail(event.target.value)}
	                							required
                							/>
            							</Form.Group>

            							<Form.Group controlId="password1">
                							<Form.Label>Password</Form.Label>
                							<							Form.Control 
	                							type="password" 
	                							placeholder="Password" 
	                							value = {password1}
	                							onChange = {event => setPassword1(event.target.value)}
	                							required
                							/>
            							</Form.Group>

            							<br/>
            							{ isActive ? // true
            							<Button variant="success" type="submit" id="submitBtn" >
            								Login
            							</Button>
            							: // false output
            							<Button variant="secondary" type="submit" id="submitBtn" disabled >
            								Login
            							</Button>
        								}
        							</Form>
        </div>
      </div>
    </div>
	)
}