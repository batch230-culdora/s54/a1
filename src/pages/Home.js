import { Fragment } from 'react';
import Banner from '../components/Banner';
import Highlights from '../components/Highlights';

const data = {
	title: "Zuitt Coding BootCamp",
	content: "Opporturnities for everyone, everywhere.",
	destination: "/courses",
	label: "Enroll Now!"

}
export default function Home() {
	return(
		<Fragment>
			<Banner data={data}/>
			<Highlights />
		</Fragment>
	)
}
